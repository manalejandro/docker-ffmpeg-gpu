# docker-ffmpeg-gpu

Use `ffmpeg` with `docker` and `nvidia` powers to transcode in Debian 11 Bullseye, based on [this nice docker project](https://git.archive.org/www/ffmpeg-gpu)

                 (__) 
                 (oo) 
           /------\/ 
          / |    ||   
         *  /\---/\ 
            ~~   ~~   
	..."Have you mooed today?"...

## Requisites

### This project needs "docker" and "docker compose" working with [nvidia runtime](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html) and the nvidia [official drivers](https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html)

## Libraries

	$ sudo apt install libnvidia-encode1

## Build

	$ git clone https://gitlab.com/manalejandro/docker-ffmpeg-gpu
	$ cd docker-ffmpeg-gpu && docker-compose build --force-rm

## Usage

### You can use "/ffmpeg" or "/ffmpeg-pascal" for architecture version

	$ docker-compose up -d
	$ docker-compose run --rm --entrypoint /ffmpeg nvidia-ffmpeg -hwaccels -v 0

	Hardware acceleration methods:
	vdpau
	cuda
	vaapi
	
## Sample using CUDA for encoding:

	$ docker-compose run --rm --entrypoint /ffmpeg nvidia-ffmpeg -vsync 0 -i /folder/input -c:a copy -c:v h264_nvenc /folder/output

## Full hardware transcode with NVDEC and NVENC:

	$ docker-compose run --rm --entrypoint /ffmpeg nvidia-ffmpeg -vsync 0 -hwaccel nvdec -hwaccel_output_format cuda -extra_hw_frames 10 -i /folder/input -c:a copy -c:v h264_nvenc /folder/output

## Shutdown

	$ docker-compose down

## License

	MIT
